﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Manager : MonoBehaviour {


	private Texture2D myCanvas;


	public Vector3 LightPosition = new Vector3(5.0f,5.0f,0.0f);


    //public Vector3[] LightList;

	//LightPower(スペクトル)
    public Spectrum LightPower = new Spectrum (3000.0f, 3000.0f, 3000.0f);
	
    //拡散光（反射率）0〜100パーセント
	//public Spectrum diffuseColor = new Spectrum (0.0f, 0.7f, 0.25f); 

	//DefalutDefineColor
	public Spectrum BLACK = new Spectrum (0.0f, 0.0f, 0.0f);
	public Spectrum WHITE = new Spectrum (1.0f, 1.0f, 1.0f);
    public Spectrum RED = new Spectrum(1.0f, 0.0f, 0.0f);
    //static const Color BLACK = new Spectrum (0.0f, 0.0f, 0.0f);

    public int DEPTH_MAX = 1000;
    //double EPSILON = 0.001;
    Vector3 EPSILON = new Vector3(0.001f,0.001f,0.001f);

    public float VACUUM_REFRACTIVE_INDEX = 1.0f; // 真空の屈折率

  

	// Use this for initialization
	void Start () {
		myCanvas = new Texture2D(Screen.width, Screen.height);	// ゲーム実行時のスクリーンサイズと同じにしておく
		Rendering();

		ScreenCapture.CaptureScreenshot(Application.dataPath + "/ScreenShot/savedata.PNG");


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Rendering()
	{
		Color backgroundColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);

        Vector3 org = Camera.main.transform.position;

		for (int h = 0; h < myCanvas.height; h++) {
			for (int w = 0; w < myCanvas.width; w++) {

				Color c = backgroundColor;

				//---- 各ピクセルの色を，当たったかどうかで変更する -----
				Ray ray  = Camera.main.ScreenPointToRay(new Vector3(w, h, 0.0f));	// カメラからのレイの生成

                /*

				RaycastHit hit;
				if (Physics.Raycast(ray, out hit)) {
					//c = new Color(1.0f, 1.0f, 1.0f, 1.0f);  // 当たった場合は白（当たらない場合は変更なし）

					//float brightness = diffuseLighting (hit.point,hit.normal.normalized,LightPosition,LightPower);

					var material = hit.collider.GetComponent<Renderer>().material;

					Spectrum diffuseColor = new Spectrum(material.color.r, material.color.g, material.color.b);
				
					//float i = Mathf.Min((int)(brightness * 255), 255);
					//c = new Color(i / 255, i / 255, i / 255, 1.0f);


					Spectrum l = diffuseLighting (hit.point, hit.normal, LightPosition, LightPower, diffuseColor);

					//l.CallTest ();
					c = l.toColor();

					Debug.Log(hit.normal);
				}
				*/

                c = trace(ray,0,Camera.main.transform.position).toColor();

				myCanvas.SetPixel(w, h, c);
			}
		}

		myCanvas.Apply();
	}

	//交点：p，法線：n
	Spectrum diffuseLighting(Vector3 p,Vector3 n,Vector3 lightPos,Spectrum lightPower,Spectrum diffuseColor){
		Vector3 v = lightPos - p;
		Vector3 l = v.normalized;

		float dot = Vector3.Dot(l,n);


		if (dot > 0) {
			if (visiable (p, lightPos)) {
				float r = v.magnitude;
				float factor = dot / (4 * Mathf.PI * r * r);
				return lightPower.scale (factor).mul (diffuseColor);
			} else {
                return BLACK;
			}

		} else {
			return BLACK;
		}
	}

	bool visiable(Vector3 org, Vector3 target){

        Vector3 v = (target - org).normalized;
        //Vector3 v = (target - org);
        //org.add(v * new Vector3(EPSILON,EPSILON,EPSILON));
        //org = org + EPSILON;

        Ray shadowRay = new Ray (org, v);

		RaycastHit ShadowRayHit;

		if(Physics.Raycast(shadowRay,out ShadowRayHit)){
			return false;
		}
		return true;
	}
		
    Vector3 reflect(Vector3 v,Vector3 n){
        float dotVN = Vector3.Dot(v, n);
        return v - (2 * n * dotVN);
    }

    Vector3 refract(Vector3 v,Vector3 n,float eta){

        //Vector3 uv = v.normalized;
        float dot = Vector3.Dot(v, n);

        //float d = 1.0f - (Mathf.Sqrt(eta) * ( 1.0f - Mathf.Sqrt(dot)));
        float d = 1.0f - Mathf.Pow(eta, 2) * (1.0f - Mathf.Pow(dot, 2));

        //d = 0.0f;
        //d = 0.8f;

        if (0 < d)
        {
            Vector3 a = v - ((n * dot) * eta);
            Vector3 b = n * Mathf.Sqrt(d);
            return a - b;

            //return -eta * (uv - n * dot) - n * Mathf.Sqrt(d);
        }
        return reflect(v,n);
        
    }





    Spectrum lighting(Vector3 p,Vector3 n,Spectrum m){
        Spectrum L = BLACK;
        L = diffuseLighting(p,n,LightPosition,LightPower,m);
        return L;
    }



    Spectrum trace(Ray ray, int depth, Vector3 org)
    {
        if (depth > DEPTH_MAX) { return BLACK; }

        RaycastHit hit;

        if (!(Physics.Raycast(ray, out hit)))
        {
            return BLACK;
        }

        Spectrum l = BLACK;

        var material = hit.collider.GetComponent<Renderer>().material;
        Spectrum diffuseColor = new Spectrum(material.color.r, material.color.g, material.color.b);

        float ks = hit.collider.GetComponent<Material>().ReflectRate;
        float kt = hit.collider.GetComponent<Material>().refractive;

        Vector3 v = hit.point - org;

        if (Vector3.Dot(hit.normal, ray.direction) < 0)
        {

            if (0 < ks)
            {
                //Vector3 v = hit.point - org;
                Vector3 r = reflect(v, hit.normal);
                Spectrum c = trace(new Ray(hit.point + (v * 0.001f), r), depth + 1, hit.point);
                l = l.add(c.scale(ks)).mul(diffuseColor);
            }

            if (0 < kt)
            {
                //Debug.Log("kt");
                //Vector3 v = hit.point - org;
                Vector3 r = refract(v, hit.normal, VACUUM_REFRACTIVE_INDEX / hit.collider.GetComponent<Material>().refractiveIndex); // 屈折レイを導出
                //Debug.Log(r);
                Spectrum c = trace(new Ray(hit.point + (v * 0.001f), r), depth + 1, hit.point);
                l = l.add(c.scale(kt)).mul(diffuseColor);
            }

            float kd = 1 - ks - kt;

            if (0 < kd)
            {
                Spectrum c = lighting(hit.point + (v * 0.001f), hit.normal, diffuseColor);
                l = l.add(c.scale(kd));
            }
        }
        else{
            //Vector3 v = hit.point - org;
            Vector3 r = refract(v, -hit.normal, hit.collider.GetComponent<Material>().refractiveIndex / VACUUM_REFRACTIVE_INDEX); // 屈折レイを導出
            l = trace(new Ray(hit.point + (v * 0.001f), r), depth + 1, hit.point);
        }



        return l;
    }
	
	void OnGUI() {
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), myCanvas);
	}


}
/*************Spectrum***************/
public class Spectrum{
	float r,g,b;

	//static const Spectrum BLACK = new Spectrum (0.0f, 0.0f, 0.0f);

	public Spectrum(float r, float g, float b){
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public Spectrum add(Spectrum v){
		return new Spectrum (this.r + v.r, this.g + v.g, this.b + v.b);
	}

	public Spectrum mul(Spectrum v){
		return new Spectrum (this.r * v.r, this.g * v.g, this.b * v.b);
	}

	public Spectrum scale(float s){
		return new Spectrum(this.r * s, this.g * s, this.b * s);
	}

	/*
	public Color toBlack(){
		return new Color (0.0f, 0.0f, 0.0f, 1.0f);
	}
	*/
	public Color toColor(){
		float ir = Mathf.Min (this.r * 255.0f, 255.0f);
		float ig = Mathf.Min (this.g * 255.0f, 255.0f);
		float ib = Mathf.Min (this.b * 255.0f, 255.0f);
		return new Color(ir / 255.0f, ig / 255.0f, ib / 255.0f, 1.0f);
		//Color rColor = new Color(1.0f,1.0f,1.0f,1.0f);
		//return  new Color(1.0f, 1.0f, 1.0f, 1.0f);
	}

	public void CallTest(){
		Debug.Log ("calling");
	}
		
} 




